import React, { Component } from "react";
import Counter from "./counter";

// class Counters extends Component {
//   render() {
//     return (
//       <div>
//         <button onClick={this.props.onReset} className="btn-primary btn-sm m2">
//           Reset
//         </button>
//         {this.props.counters.map(counter => (
//           <Counter
//             key={counter.id}
//             onDelete={this.props.onDelete}
//             onIncrement={this.props.onIncrement}
//             counter={counter}
//           >
//             <h4>Title #{counter.id}</h4>
//           </Counter>
//         ))}
//       </div>
//     );
//   }
// }

// this를 치환해서 처리 함
class Counters extends Component {
  render() {
    const { onClick, onDelete, onIncrement, onReset, counters } = this.props;
    return (
      <div>
        <button onClick={onReset} className="btn-primary btn-sm m2">
          Reset
        </button>
        {counters.map(counter => (
          <Counter
            key={counter.id}
            onDelete={onDelete}
            onIncrement={onIncrement}
            counter={counter}
          >
            <h4>Title #{counter.id}</h4>
          </Counter>
        ))}
      </div>
    );
  }
}

export default Counters;
